//
//  DataManagerViewController.swift
//  test-bankapp-ios-endcom
//
//  Created by Americo MQ on 06/08/21.
//

import Foundation

struct DataManagerViewController {
    func parseAccountsJson(comp: @escaping (AccountsDataModel) -> ()) {
        if let url = URL(string: "http://bankapp.endcom.mx/api/bankappTest/cuenta") {
            URLSession.shared.dataTask(with: url) {
                data, response, error in
                if error != nil {
                    print("Error \(error?.localizedDescription)")
                }
                do {
                    let result = try JSONDecoder().decode(AccountsDataModel.self, from: data!)
                    comp(result)
                } catch {
                    print("error \(error.localizedDescription)")
                }
            }.resume()
        }
    }
    func parseBalanceJson(comp: @escaping (BalanceDataModel) -> ()) {
        if let url = URL(string: "http://bankapp.endcom.mx/api/bankappTest/saldos") {
            URLSession.shared.dataTask(with: url) {
                data, response, error in
                if error != nil {
                    print("Error \(error?.localizedDescription)")
                }
                do {
                    let result = try JSONDecoder().decode(BalanceDataModel.self, from: data!)
                    comp(result)
                } catch {
                    print("error \(error.localizedDescription)")
                }
            }.resume()
        }
    }
    func parseCardsJson(comp: @escaping (CardsDataModel) -> ()) {
        if let url = URL(string: "http://bankapp.endcom.mx/api/bankappTest/tarjetas") {
            URLSession.shared.dataTask(with: url) {
                data, response, error in
                if error != nil {
                    print("Error \(error?.localizedDescription)")
                }
                do {
                    let result = try JSONDecoder().decode(CardsDataModel.self, from: data!)
                    comp(result)
                } catch {
                    print("error \(error.localizedDescription)")
                }
            }.resume()
        }
    }
    func parseMovementsJson(comp: @escaping (MovementsDataModel) -> ()) {
        if let url = URL(string: "http://bankapp.endcom.mx/api/bankappTest/movimientos") {
            URLSession.shared.dataTask(with: url) {
                data, response, error in
                if error != nil {
                    print("Error \(error?.localizedDescription)")
                }
                do {
                    let result = try JSONDecoder().decode(MovementsDataModel.self, from: data!)
                    comp(result)
                } catch {
                    print("error \(error.localizedDescription)")
                }
            }.resume()
        }
    }
}
