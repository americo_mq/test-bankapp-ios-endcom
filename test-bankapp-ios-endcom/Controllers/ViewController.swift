//
//  ViewController.swift
//  test-bankapp-ios-endcom
//
//  Created by Americo MQ on 05/08/21.
//

import UIKit

class ViewController: UIViewController {
    
//    var fruits = ["apple","strawberry","pineapple"]
    let dataManager = DataManagerViewController()

    @IBOutlet weak var topView: TopView!
    @IBOutlet weak var secondView: TopView!
    @IBOutlet weak var cardsView: CardsView!
    @IBOutlet weak var thirdView: TopView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableView1: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        tableView.dataSource = self
        tableView.delegate = self
        tableView1.dataSource = self
        
        self.navigationItem.titleView = navTitleWithImageAndText(titleText: "BankApp", imageName: "circle")
        getData()
    }
    
    func navTitleWithImageAndText(titleText: String, imageName: String) -> UIView {

        // Creates a new UIView
        let titleView = UIView()

        // Creates a new text label
        let label = UILabel()
        label.text = titleText
        label.sizeToFit()
        label.textColor = #colorLiteral(red: 0, green: 0.6764723659, blue: 0.4598798156, alpha: 1)
        label.center = titleView.center
        label.textAlignment = NSTextAlignment.center

        // Creates the image view
        let image = UIImageView()
        image.tintColor = #colorLiteral(red: 0, green: 0.6764723659, blue: 0.4598798156, alpha: 1)
        image.image = UIImage(systemName: imageName)

        // Maintains the image's aspect ratio:
        let imageAspect = image.image!.size.width / image.image!.size.height

        // Sets the image frame so that it's immediately before the text:
        let imageX = label.frame.origin.x - label.frame.size.height * imageAspect
        let imageY = label.frame.origin.y

        let imageWidth = label.frame.size.height * imageAspect
        let imageHeight = label.frame.size.height

        image.frame = CGRect(x: imageX, y: imageY, width: imageWidth, height: imageHeight)

        image.contentMode = UIView.ContentMode.scaleAspectFit

        // Adds both the label and image view to the titleView
        titleView.addSubview(label)
        titleView.addSubview(image)

        // Sets the titleView frame to fit within the UINavigation Title
        titleView.sizeToFit()

        return titleView

    }
    
    func configureView() {
        //Top View Configuration
        topView.containerView.backgroundColor = .blue
        topView.firstLabel.text = "Juanito peerez"
        topView.secondLabel.text = "vvsbdwdeoidjeodje"
        //Second View Configuration
        secondView.backgroundColor = .white
        let multipleAttributes: [NSAttributedString.Key : Any] = [ NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue, NSAttributedString.Key.foregroundColor: UIColor.systemBlue ]
        let firtAttrString = NSAttributedString(string: "Mis Cuentas", attributes: multipleAttributes)
        let multipleAttributesForSecondString: [NSAttributedString.Key : Any] = [ NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue, NSAttributedString.Key.foregroundColor: UIColor.systemGray ]
        let secondAttrString = NSAttributedString(string: "Enviar Dinero", attributes: multipleAttributesForSecondString)
        secondView.firstLabel.attributedText = firtAttrString
        secondView.secondLabel.attributedText = secondAttrString
        //Cards View Configuration
        cardsView.firstLabel.text = "Saldo general en tus cuentas"
        cardsView.secondLabel.text = "$2,000.00"
        //Third View Configuration
        let thirdAttrString = NSAttributedString(string: "+Agregar una tarjeta de debito o credito", attributes: multipleAttributes)
        thirdView.firstLabel.attributedText = thirdAttrString
        thirdView.secondLabel.text = ""
        //tableView Configuration
        tableView.register(UINib(nibName: "CardInfoTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        //tableView1 Configuration
        tableView1.register(UINib(nibName: "MovementsTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
    }

    func getData() {
        dataManager.parseMovementsJson { (data) in
            for i in data.movements {
                print(i.ammount,i.date,i.type)
            }
        }
    }

}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
//        var count:Int?
//
//        if tableView == self.tableView {
//            count = 2
//        }
//
//        if tableView == self.tableView1 {
//            count =  2
//        }
//
//        return count!
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        
        if tableView == self.tableView {
            let cell: CardInfoTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CardInfoTableViewCell
            cell.cardImage?.backgroundColor = .red
            cell.statusLabel?.text = "Activa"
            cell.accountNumberLabel?.text = "2345 3465 4567 5677"
            cell.nameLabel?.text = "Carolina lopex lopez"
            cell.propertyLabel?.text = "Titulae"
            cell.amountLabel?.text = "$25,000.00"
            return cell
        } else {
            let cell: MovementsTableViewCell = self.tableView1.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MovementsTableViewCell
            cell.titleLabel.text = "Titulo"
            cell.amountLabel.text = "$25,000.00"
            cell.dateLabel.text = "56/67/6789"
            return cell
        }


    }
}
extension ViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}

