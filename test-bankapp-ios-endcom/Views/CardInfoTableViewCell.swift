//
//  CardInfoTableViewCell.swift
//  test-bankapp-ios-endcom
//
//  Created by Americo MQ on 05/08/21.
//

import UIKit

class CardInfoTableViewCell: UITableViewCell {
    
    static let identifier = "cell"

    @IBOutlet weak var cardImage: UIImageView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var accountNumberLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var propertyLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
