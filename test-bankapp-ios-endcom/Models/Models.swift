//
//  Models.swift
//  test-bankapp-ios-endcom
//
//  Created by Americo MQ on 06/08/21.
//

import Foundation

struct AccountsDataModel: Decodable {
    let accounts: [Accounts]
    
    private enum CodingKeys: String, CodingKey {
        case accounts = "cuenta"
    }
}

struct Accounts: Decodable {
    let name: String?
    let lastSession: String?
    
    private enum CodingKeys: String, CodingKey {
        case name = "nombre"
        case lastSession = "ultimaSesion"
    }
}

struct BalanceDataModel: Decodable {
    let balance: [Balance]
    
    private enum CodingKeys: String, CodingKey {
        case balance = "saldos"
    }
}

struct Balance: Decodable {
    let income: Int?
    let expense: Int?
    let generalBalance: Int?
    
    private enum CodingKeys: String, CodingKey {
        case income = "ingresos"
        case expense = "gastos"
        case generalBalance = "saldoGeneral"
    }
}

struct CardsDataModel: Decodable {
    let cards: [Cards]
    
    private enum CodingKeys: String, CodingKey {
        case cards = "tarjetas"
    }
}

struct Cards: Decodable {
    
    let card: String?
    let name: String?
    let balance: Int?
    let status: String?
    let type: String?
    
    private enum CodingKeys: String, CodingKey {
        case card = "tarjeta"
        case name = "nombre"
        case balance = "saldo"
        case status = "estado"
        case type = "tipo"
    }
}

struct MovementsDataModel: Decodable {
    let movements: [Movements]
    
    private enum CodingKeys: String, CodingKey {
        case movements = "movimientos"
    }
}

struct Movements: Decodable {
    let date: String?
    let ammount: String?
    let type: String?
    
    private enum CodingKeys: String, CodingKey {
        case date = "fecha"
        case ammount = "monto"
        case type = "tipo"
    }
}
